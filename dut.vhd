----------------------------------------------------------
	-- Test Device Tick Gen 1KHz
	--
	--@praktikum	CEP
	--@semester		WS13
	--@teamname		S3T4
	--@name			Schaeufler, Jonas 	#2092427
	--				   Bergmann, Jonas		#2045479
	--@aufgabe		Aufgabe A1
	--@kontrolleur	Prof. Dr. Schaaefers, Michael
-----------------------------------------------------------

library work;
	use work.all;
library ieee;
	use ieee.std_logic_1164.all;
	
entity dut is
  port (
    clk50m : in std_logic;
    sreset : in std_logic;
    seg7a : out std_logic_vector(3 downto 0);
    seg7c : out std_logic_vector(7 downto 0)
  
  );
end entity dut;

architecture arc of dut is



	signal clk_1khz : std_logic;
	signal clk_1hz : std_logic;
	signal value : std_logic_vector(15 downto 0);

	


	component gTickGen is
	  generic (
	     nob : natural := 16;          -- Number Of Bits of (ticki) cycle counter
        noc : natural := 50000        -- Number Of (ticki) Cycles
    ); -- generic

    port (
        ticko    : out std_logic;     -- TICK (with reduced frequency) Outgoing

        ticki    : in std_logic;      -- TICK/enable Incoming

        clk      : in std_logic;      -- CLocK
        sres     : in std_logic       );-- Synchronous RESET
	 end component gTickGen;
	 for all : gTickGen use entity work.gTickGen(rtl);

  component counter is
  generic (
    
    nob : natural := 16;          -- Number Of Bits of (ticki) cycle counter
	noc : natural := 255       -- Number Of (ticki) Cycles

    ); -- generic
  port (
    ticki : in std_logic;
    clk : in std_logic;
    sreset : in std_logic;
    value : out std_logic_vector(15 downto 0)
    
  );
  
end component counter;
for all : counter use entity work.counter(beh);

component display_hex is

	port (
		ticki : in std_logic;
		clk      	  : in std_logic;
		sres 	      : in std_logic;
		value  	  : in std_logic_vector(15 downto 0);
		seg7a	  : out std_logic_vector(3 downto 0);
		seg7c 	  : out std_logic_vector(7 downto 0)
	);
		
end component display_hex;
for all : display_hex use entity work.display_hex(arc);
  

begin

	ticker_1khz_i : gTickGen
	generic map ( noc => 49999, nob => 16 )
	port map (
		  ticki => '1',
		  sres => sreset,
		  ticko => clk_1khz,
		  clk => clk50m	  
	 );
	  
	ticker_1hz_i : gTickGen
	generic map ( noc => 999, nob => 10 )
	port map (
		  ticki => clk_1khz,
		  sres => sreset,
		  ticko => clk_1hz,  
		  clk => clk50m
	  );

	 counter_i : counter
	 generic map( noc => 255, nob => 16)
	 port map(
		
			ticki => clk_1hz,
		   clk => clk50m,
		   sreset => sreset,
		   value => value
	   );
	 display_counter_i : display_hex
	 port map(
			ticki => clk_1khz,
		   clk => clk50m,
		   sres => sreset,
		   value => value,
		   seg7a => seg7a,
		   seg7c => seg7c
	);  
	
	
end architecture arc;

