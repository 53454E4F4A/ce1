onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Counter
add wave -noupdate -radix hexadecimal -childformat {{/dut/value_out(15) -radix hexadecimal} {/dut/value_out(14) -radix hexadecimal} {/dut/value_out(13) -radix hexadecimal} {/dut/value_out(12) -radix hexadecimal} {/dut/value_out(11) -radix hexadecimal} {/dut/value_out(10) -radix hexadecimal} {/dut/value_out(9) -radix hexadecimal} {/dut/value_out(8) -radix hexadecimal} {/dut/value_out(7) -radix hexadecimal} {/dut/value_out(6) -radix hexadecimal} {/dut/value_out(5) -radix hexadecimal} {/dut/value_out(4) -radix hexadecimal} {/dut/value_out(3) -radix hexadecimal} {/dut/value_out(2) -radix hexadecimal} {/dut/value_out(1) -radix hexadecimal} {/dut/value_out(0) -radix hexadecimal}} -subitemconfig {/dut/value_out(15) {-radix hexadecimal} /dut/value_out(14) {-radix hexadecimal} /dut/value_out(13) {-radix hexadecimal} /dut/value_out(12) {-radix hexadecimal} /dut/value_out(11) {-radix hexadecimal} /dut/value_out(10) {-radix hexadecimal} /dut/value_out(9) {-radix hexadecimal} /dut/value_out(8) {-radix hexadecimal} /dut/value_out(7) {-radix hexadecimal} /dut/value_out(6) {-radix hexadecimal} /dut/value_out(5) {-radix hexadecimal} /dut/value_out(4) {-radix hexadecimal} /dut/value_out(3) {-radix hexadecimal} /dut/value_out(2) {-radix hexadecimal} /dut/value_out(1) {-radix hexadecimal} /dut/value_out(0) {-radix hexadecimal}} /dut/value_out
add wave -noupdate -divider {Seven segments}
add wave -noupdate -radix binary -childformat {{/dut/seg7c(7) -radix binary} {/dut/seg7c(6) -radix binary} {/dut/seg7c(5) -radix binary} {/dut/seg7c(4) -radix binary} {/dut/seg7c(3) -radix binary} {/dut/seg7c(2) -radix binary} {/dut/seg7c(1) -radix binary} {/dut/seg7c(0) -radix binary}} -expand -subitemconfig {/dut/seg7c(7) {-radix binary} /dut/seg7c(6) {-radix binary} /dut/seg7c(5) {-radix binary} /dut/seg7c(4) {-radix binary} /dut/seg7c(3) {-radix binary} /dut/seg7c(2) {-radix binary} /dut/seg7c(1) {-radix binary} /dut/seg7c(0) {-radix binary}} /dut/seg7c
add wave -noupdate -divider digit
add wave -noupdate -radix binary -childformat {{/dut/seg7a(3) -radix binary} {/dut/seg7a(2) -radix binary} {/dut/seg7a(1) -radix binary} {/dut/seg7a(0) -radix binary}} -radixenum numeric -subitemconfig {/dut/seg7a(3) {-radix binary -radixenum numeric} /dut/seg7a(2) {-radix binary -radixenum numeric} /dut/seg7a(1) {-radix binary -radixenum numeric} /dut/seg7a(0) {-radix binary -radixenum numeric}} /dut/seg7a
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {292 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 92
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1912 ns}
