library work;
	use work.all;
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  
entity counter is
    
	generic (
    
    nob : natural := 16;          -- Number Of Bits of (ticki) cycle counter
	noc : natural := 255       -- Number Of (ticki) Cycles

    ); -- generic
	
	port (
    
	ticki : in std_logic;
    clk : in std_logic;
    sreset : in std_logic;
    value : out std_logic_vector(15 downto 0)
    
	);
    
	constant mpo : natural := nob-1;  -- Most significant bit POsition
    constant max : natural := noc-1;  -- MAXimum value of counter

	end entity counter;

architecture beh of counter is
   signal value_ns : std_logic_vector(mpo downto 0);
 	signal value_cs : std_logic_vector(mpo downto 0) := (others => '0');
begin


sequilo: process(clk) is

begin	
		
		if clk'event and clk = '1' then	
			if sreset = '1' then
				value_cs <= (others => '0');
				else
				value_cs <= value_ns;			
			end if;
		end if;

	
end process sequilo;

value <= value_cs;
  
cobilo : process(value_cs, ticki) is
	variable value_v : std_logic_vector(mpo downto 0) := (others => '0');
begin
	
	value_v := value_cs;
    
	if(ticki = '1') then
		if(value_v = max) then
			value_v := (others => '0'); 	
		else
		    value_v := (value_cs + 1);
		end if;	   		   
	end if;		
			
	value_ns <= value_v;	
		
		
    end process cobilo;
  
end architecture beh;