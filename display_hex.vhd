----------------------------------------------------------
	-- Counter and Display Update
	--
	--@praktikum	CEP
	--@semester		WS13
	--@teamname		S3T4
	--@name			Schaeufler, Jonas 	#2092427
	--				   Bergmann, Jonas		#2045479
	--@aufgabe		Aufgabe A1
	--@kontrolleur	Prof. Dr. Schaefers, Michael
-----------------------------------------------------------

library work;
	use work.all;
library ieee;
	use ieee.std_logic_1164.all;	
	use ieee.numeric_std.all;
	use ieee.std_logic_unsigned.all;

	
	
entity display_hex is

	port (
		ticki : in std_logic;
		clk       : in std_logic;
		sres 	   	: in std_logic;
		value     : in std_logic_vector(15 downto 0);
		seg7a	   	: out std_logic_vector(3 downto 0);
		seg7c 	   : out std_logic_vector(7 downto 0)
	);
		
end display_hex;
	

architecture arc of display_hex is

	
type hex_to_7seg_table is array( natural range <> ) of std_logic_vector( 7 downto 0 ); 
   
    constant h7s_table : hex_to_7seg_table( 0 to 15 ) := (
    -- ------------------------------------------------+---|
    -- |  l    f    d    d    g    j    h    c         |   |
    -- |  1    1    1    1    1    1    1    1         |   |
    -- |  8    8    7    6    4    7    4    7         |   |
    -- ------------------------------------------------+---|
	   ( '0', '0', '0', '0', '0', '0', '1', '1' ),  -- | 0 |
	   ( '1', '0', '0', '1', '1', '1', '1', '1' ),	-- | 1 |
	   ( '0', '0', '1', '0', '0', '1', '0', '1' ),  -- | 2 |
	   ( '0', '0', '0', '0', '1', '1', '0', '1' ), 	-- | 3 |
	   ( '1', '0', '0', '1', '1', '0', '0', '1' ), 	-- | 4 |
	   ( '0', '1', '0', '0', '1', '0', '0', '1' ), 	-- | 5 |
	   ( '0', '1', '0', '0', '0', '0', '0', '1' ),	-- | 6 |
	   ( '0', '0', '0', '1', '1', '1', '1', '1' ), 	-- | 7 |
	   ( '0', '0', '0', '0', '0', '0', '0', '1' ),	-- | 8 |
	   ( '0', '0', '0', '0', '1', '0', '0', '1' ), 	-- | 9 |
	   ( '0', '0', '0', '1', '0', '0', '0', '1' ),	-- | a |
	   ( '1', '1', '0', '0', '0', '0', '0', '1' ),	-- | b |
	   ( '0', '1', '1', '0', '0', '0', '1', '1' ),	-- | c |
	   ( '1', '0', '0', '0', '0', '1', '0', '1' ),	-- | d |
	   ( '0', '1', '1', '0', '0', '0', '0', '1' ),	-- | e |
	   ( '0', '1', '1', '1', '0', '0', '0', '1' )	-- | f |
	   ------------------------------------------------+---|
    --    c    c    c    c    c    c    c    d         |   |
    --    a    b    c    d    e    f    g    p         |   |
    -- -----------------------------------------------------
 );
    
signal seg7a_ns : std_logic_vector(3 downto 0);
signal seg7c_ns : std_logic_vector(7 downto 0);

signal seg7a_cs : std_logic_vector(3 downto 0) := "0111";
signal seg7c_cs : std_logic_vector(7 downto 0) := (others => '1');

signal digit_cs : std_logic_vector(3 downto 0) := "1110";
signal digit_ns : std_logic_vector(3 downto 0);
begin

  
seg7a <= seg7a_cs;
seg7c <= seg7c_cs;

sequilo: process (clk) is
   begin
	  if clk'event and clk = '1' then	
			if sres = '1' then
				seg7a_cs <= (others => '0');
				seg7c_cs <= (others => '0');
				digit_cs <= digit_ns;
			else
				seg7a_cs <= seg7a_ns;
				seg7c_cs <= seg7c_ns;	
				digit_cs <= digit_ns;		end if;
	end if;
end process sequilo;



cobilo: process (value, digit_cs, ticki, seg7a_cs, seg7c_cs) is
	variable digit_v : std_logic_vector(3 downto 0);
	variable seg7c_v : std_logic_vector(7 downto 0);
	variable seg7a_v : std_logic_vector(3 downto 0);
	
  begin
			
	digit_v := digit_cs;
	seg7c_v := seg7c_cs;
	seg7a_v := seg7a_cs;
       
	if(ticki = '1') then
		
		if digit_cs = "1110" then 			
		  
			seg7c_v := h7s_table(to_integer(unsigned(value(3 downto 0))));
			seg7a_v := digit_cs;
			digit_v := "1101";

		elsif digit_cs = "1101" then 		

			seg7c_v := h7s_table(to_integer(unsigned(value(7 downto 4))));
			seg7a_v := digit_cs;				
			digit_v := "1011";

		elsif digit_cs = "1011" then 

			seg7c_v := h7s_table(to_integer(unsigned(value(11 downto 8))));		
			seg7a_v := digit_cs;
			digit_v := "0111";

		elsif digit_cs = "0111" then 

			seg7c_v := h7s_table(to_integer(unsigned(value(15 downto 12))));
			seg7a_v := digit_cs;
			digit_v := "1110";

		else 

			seg7c_v := x"11";
			seg7a_v := x"0";
			digit_v := "1110";		
					
         end if;
		
	end if;
		
	digit_ns <= digit_v;
	seg7c_ns <= seg7c_v;
	seg7a_ns <= seg7a_v;
		
		

		
 
  end process cobilo;
		
		
end architecture arc;


