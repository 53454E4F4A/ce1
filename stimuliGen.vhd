----------------------------------------------------------
	-- Stimuli Generator for Tick Generator Test
	--
	--@praktikum	CEP
	--@semester		WS13
	--@teamname		S3T4
	--@name			Schaeufler, Jonas 	#0000000
	--				   Bergmann, Jonas		#2045479
	--@aufgabe		Aufgabe A1
	--@kontrolleur	Prof. Dr. Schaaefers, Michael
-----------------------------------------------------------

library work;
	use work.all;
library ieee;
	use ieee.std_logic_1164.all;
	
ENTITY stimuliGen IS
	PORT (
		clk	: OUT	STD_LOGIC;
		nres	: OUT	STD_LOGIC
	);
END ENTITY stimuliGen;

ARCHITECTURE stimuliGen_a OF stimuliGen IS
	
	SIGNAL run_s : BOOLEAN;

BEGIN

runTime:
	PROCESS IS
	
	BEGIN
		run_s <= TRUE;
		WAIT FOR 100000 ns;
		run_s <= FALSE;
		WAIT;
	END PROCESS runTime;
	
reset:
	PROCESS IS
	BEGIN
		nres <= '0';
		WAIT;
	END PROCESS reset;
	
clock:
	PROCESS IS
	
	BEGIN
		WAIT FOR 1 ns;
		WHILE run_s LOOP
			clk <= '1';
			WAIT FOR 10 ns;
			clk <= '0';
			WAIT FOR 10 ns;
		END LOOP;
		WAIT;
	END PROCESS clock;
END ARCHITECTURE stimuliGen_a;
