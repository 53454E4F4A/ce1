onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Display
add wave -noupdate -expand /dut/seg7a
add wave -noupdate -expand /dut/seg7c
add wave -noupdate -divider Counter
add wave -noupdate -radix decimal -childformat {{/dut/value_out(15) -radix decimal} {/dut/value_out(14) -radix decimal} {/dut/value_out(13) -radix decimal} {/dut/value_out(12) -radix decimal} {/dut/value_out(11) -radix decimal} {/dut/value_out(10) -radix decimal} {/dut/value_out(9) -radix decimal} {/dut/value_out(8) -radix decimal} {/dut/value_out(7) -radix decimal} {/dut/value_out(6) -radix decimal} {/dut/value_out(5) -radix decimal} {/dut/value_out(4) -radix decimal} {/dut/value_out(3) -radix decimal} {/dut/value_out(2) -radix decimal} {/dut/value_out(1) -radix decimal} {/dut/value_out(0) -radix decimal}} -subitemconfig {/dut/value_out(15) {-height 15 -radix decimal} /dut/value_out(14) {-height 15 -radix decimal} /dut/value_out(13) {-height 15 -radix decimal} /dut/value_out(12) {-height 15 -radix decimal} /dut/value_out(11) {-height 15 -radix decimal} /dut/value_out(10) {-height 15 -radix decimal} /dut/value_out(9) {-height 15 -radix decimal} /dut/value_out(8) {-height 15 -radix decimal} /dut/value_out(7) {-height 15 -radix decimal} /dut/value_out(6) {-height 15 -radix decimal} /dut/value_out(5) {-height 15 -radix decimal} /dut/value_out(4) {-height 15 -radix decimal} /dut/value_out(3) {-height 15 -radix decimal} /dut/value_out(2) {-height 15 -radix decimal} /dut/value_out(1) {-height 15 -radix decimal} /dut/value_out(0) {-height 15 -radix decimal}} /dut/value_out
add wave -noupdate -divider Clocks
add wave -noupdate /dut/clk_1Hz
add wave -noupdate /dut/clk_1KHz
add wave -noupdate /dut/clk_simu
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
add wave -noupdate -divider {Display inside}
add wave -noupdate /dut/display_counter_i/value
add wave -noupdate -divider wert
add wave -noupdate -expand /dut/display_counter_i/seg7a_cs
add wave -noupdate -expand /dut/display_counter_i/seg7c_cs
add wave -noupdate -divider stelle
add wave -noupdate /dut/display_counter_i/seg7a_ns
add wave -noupdate /dut/display_counter_i/seg7c_ns
add wave -noupdate /dut/display_counter_i/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {663 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 249
configure wave -valuecolwidth 124
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {937 ns}
