onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /dut/clk_1hz
add wave -noupdate /dut/clk_1khz
add wave -noupdate /dut/clk_simu
add wave -noupdate -divider Ticker1
add wave -noupdate /dut/ticker_1hz_i/ticko
add wave -noupdate /dut/ticker_1hz_i/ticki
add wave -noupdate /dut/ticker_1hz_i/clk
add wave -noupdate /dut/ticker_1hz_i/ticko_cs
add wave -noupdate /dut/ticker_1hz_i/ticko_ns
add wave -noupdate -radix decimal /dut/ticker_1hz_i/count_cs
add wave -noupdate -divider Ticker2
add wave -noupdate /dut/ticker_1khz_i/ticko
add wave -noupdate /dut/ticker_1khz_i/ticki
add wave -noupdate /dut/ticker_1khz_i/clk
add wave -noupdate /dut/ticker_1khz_i/ticko_cs
add wave -noupdate /dut/ticker_1khz_i/ticko_ns
add wave -noupdate -radix decimal /dut/ticker_1khz_i/count_cs
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {391 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 342
configure wave -valuecolwidth 138
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1837 ns}
